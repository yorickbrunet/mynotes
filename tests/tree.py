"""Info about test tree."""

TESTS_DIR = 'tests'

ROOT = 'root'
FILE_W_TITLE = 'file_w_title.md'
FILE_WO_TITLE = 'file_wo_title.md'
FILE_UNKNOWN = 'abcde'

ROOT_D1 = 'root/d1'
D1 = 'd1'
FILE3 = 'file3.md'
FILE4 = 'file4.md'

ROOT_D2 = 'root/d_2'
D2 = 'd_2'
FILE5 = 'file5.md'
FILE6 = 'file6.md'
