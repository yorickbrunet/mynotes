# mynotes builds indexes and converts to HTML a tree of notes written in
# Markdown.
# Copyright (C) 2018 Yorick Brunet <mynotes@yok.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

SRC_DIR = mynotes
TST_DIR = tests
PY_VER = 3.7.1
PKG_NAME = $(SRC_DIR)
VENV_NAME = $(PKG_NAME)
CHK_VENV_NAME = "$(VENV_NAME)Test"

all:
	@echo "usage: make [clean | install | package | quality | pubtest | publish | setup | test | upgrade ]"
	@echo ""
	@echo "arguments:"
	@echo " clean      Clean unnecessary files"
	@echo " install    Install the package locally in develop mode"
	@echo " package    Create tar.gz package"
	@echo " quality    Check code quality with linters"
	@echo " pubtest    Publish on TestPyPi"
	@echo " publish    Publish on PyPi"
	@echo " setup      Set up the development environment"
	@echo " test       Run unit tests"
	@echo " upgrade    Upgrade the development environment"
	@echo ""
	@echo "Steps to follow for a new release:"
	@echo " [upgrade,] quality, test, pubtest, publish"

setup:
	pyenv virtualenv $(PY_VER) $(VENV_NAME)
	pyenv local $(VENV_NAME)
	pip install --upgrade pip
	pip install -r requirements.txt

upgrade:
	pip install --upgrade pip
	pip install -U -r requirements.txt

quality:
	flake8 $(SRC_DIR)
	pydocstyle $(SRC_DIR)
	mypy $(SRC_DIR)
	radon cc $(SRC_DIR) --min C -s
	radon mi $(SRC_DIR) --min C -s

install:
	python setup.py develop

# https://packaging.python.org/tutorials/packaging-projects/
package:
	@echo "Build package..."
	python setup.py sdist bdist_wheel
	@echo "\n\nCheck package..."
	twine check dist/*

pubtest:
	@echo "Upload package to TestPyPi..."
	twine upload --skip-existing --repository-url https://test.pypi.org/legacy/ dist/*
	@echo "\nPackage uploaded on https://test.pypi.org/manage/project/$(PKG_NAME)/releases/"
	@echo "\n\nCheck package installation:"
	@echo "pyenv virtualenv $(PY_VER) $(CHK_VENV_NAME)"
	@echo "pyenv local $(CHK_VENV_NAME)"
	@echo "pip install --upgrade pip"
	@echo "pip install --index-url https://test.pypi.org/simple/ $(PKG_NAME)"
	@echo "python -c \"import $(PKG_NAME); print($(PKG_NAME).NAME); print($(PKG_NAME).VERSION)\""
	@echo "pyenv uninstall $(CHK_VENV_NAME)"

publish:
	twine upload --skip-existing dist/*
	@echo "\nPackage uploaded on https://pypi.org/manage/project/$(PKG_NAME)/releases/"

test:
	@pytest --cov=$(SRC_DIR) $(TST_DIR)
	@coverage html

clean:
	rm -rf $(SRC_DIR)/__pycache__/ \
		   $(TST_DIR)/__pycache__/ \
		   .eggs/ \
		   dist/ \
		   $(PKG_NAME).egg-info/ \
		   htmlcov \
		   build \
		   .mypy_cache/ \
		   .pytest_cache/
